#include <vector>
#include <cmath>
#include <iostream>
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_math.h>
#include <cuda_fp16.h>
#include <cuda_fp16.hpp>

template <typename real> class devPtr{
	real *host, *device;
	public:
	devPtr(std::size_t Nelem){
		std::cout << "Error code: " << cudaMallocHost((void**)&host, Nelem * sizeof(real)) << std::endl;
		std::cout << "Error code: " << cudaMalloc((void**)&device, Nelem * sizeof(real)) << std::endl;
	}
	~devPtr(){
		cudaFreeHost(host);
		cudaFree(device);
	}
	real& operator[](std::size_t idx){
		return host[idx];
	}
};

int main(){
	typedef half real;
	const std::size_t Nelem = 1024;
	real *x, *y, *z;
	std::cout << "Error code: " << cudaMallocHost((void**)&x, Nelem * sizeof(real)) << std::endl;
	std::cout << "Error code: " << cudaMallocHost((void**)&y, Nelem * sizeof(real)) << std::endl;
	std::cout << "Error code: " << cudaMallocHost((void**)&z, Nelem * sizeof(real)) << std::endl;
	for(int i = 0 ; i < Nelem ; ++ i){
		x[i] = static_cast<real>(2.0 * (double)rand()/RAND_MAX - 1.0);
		y[i] = static_cast<real>(2.0 * (double)rand()/RAND_MAX - 1.0);
		//z[i] = x[i] + y[i];
		std::cout << (double)z[i] << " = " << (double)x[i] << " + " << (double)y[i] << std::endl;
	}
	return 0;
}

