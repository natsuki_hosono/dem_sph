#include <vector>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_math.h>
#include <cuda_fp16.h>
#include <cuda_fp16.hpp>

enum Prefix{
	No = 1,
	Ki = 1024,
	Mi = 1024 * 1024,
	Gi = 1024 * 1024 * 1024,
	G = 1000 * 1000 * 1000,
	//T = 1000 * 1000 * 1000 * 1000,
	//Ti = 1024 * 1024 * 1024 * 1024,
};

struct bandwidth{
	double byte_per_sec;
	double wallclock_time;
};

struct flops{
	double flops;
	double wallclock_time;
	double operational_intensity;
};

struct CudaTimer{
	float wc_time;
	cudaEvent_t m_start_event;
	cudaEvent_t m_stop_event;
	CudaTimer(){
		wc_time = 0.0;
		cudaEventCreate(&m_start_event);
		cudaEventCreate(&m_stop_event);
	}
	~CudaTimer(){
		cudaEventDestroy(m_start_event);
		cudaEventDestroy(m_stop_event);
	}
	void start(){
		cudaEventRecord(m_start_event, 0);
	}
	void stop(){
		cudaEventRecord(m_stop_event, 0);
		cudaEventSynchronize(m_stop_event);
		//milliseconds
		cudaEventElapsedTime(&wc_time, m_start_event, m_stop_event);
	}
	float getWallclockTime() const{
		return wc_time / 1.0e+3;
	}
};

template <typename real> class GPUptr{
	real *host, *device;
	GPUptr(const int Nelem){
		cudaMallocHost((void**)&host, Nelem * sizeof(real));
		cudaMalloc((void**)&device, Nelem * sizeof(real));
	}
	~GPUptr(){
		cudaFreeHost(host);
		cudaFree(device);
	}
};


template <typename real> __global__ void deviceStreamCopy(real* x, real* y, const int Nelem){
	const int i = blockDim.x * blockIdx.x + threadIdx.x;
	y[i] = x[i];
}

__global__ void EmptyKernel(void){
}

template <typename real> __global__ void deviceStreamAdd(real* x, real* y, real* z, const int Nelem, const int Niter){
	const int i = blockDim.x * blockIdx.x + threadIdx.x;
	real z_ = x[i];
	const real y_ = y[i];
	for(int it = 0 ; it < Niter ; ++ it) z_ += y_;
	z[i] = z_;
}
/*
template <> __global__ void deviceStreamAdd<half>(half* x, half* y, half* z, const int Nelem, const int Niter){
	const int i = blockDim.x * blockIdx.x + threadIdx.x;
	half z_ = x[i];
	const half y_ = y[i];
	for(int it = 0 ; it < Niter ; ++ it) z_ = __hadd(y_, z_);
	z[i] = z_;
}
*/
template <typename real> __global__ void deviceStreamTriad(real* x, real* y, real* z, const int Nelem, const int Niter){
	const int i = blockDim.x * blockIdx.x + threadIdx.x;
	real z_ = x[i];
	const real y_ = y[i];
	for(int it = 0 ; it < Niter ; ++ it) z_ = fma(y_, y_, z_);
	z[i] = z_;
}

template <> __global__ void deviceStreamTriad<half>(half* x, half* y, half* z, const int Nelem, const int Niter){
	const int i = blockDim.x * blockIdx.x + threadIdx.x;
	half z_ = x[i];
	const half y_ = y[i];
	for(int it = 0 ; it < Niter ; ++ it) z_ = __hfma(y_, y_, z_);
	z[i] = z_;
}

class GPUs{
	struct GPU{
		cudaDeviceProp deviceProp;
		int id;
		GPU(int _id) : id(_id){
			cudaGetDeviceProperties(&deviceProp, id);
		}
		void getQuery(){
			std::cout << "device id:" << id << std::endl;
			std::cout << " --- Name: " << deviceProp.name << std::endl;
			std::cout << " --- Total Global Memory:" << deviceProp.totalGlobalMem / 1024 / 1024 / 1024 << " GiByte" << std::endl;
			std::cout << " --- # of Streaming Multi Processors: " << deviceProp.multiProcessorCount << std::endl;
			std::cout << " --- # of cores per one Streaming Multi Proc.: " << _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor) << std::endl;
			std::cout << " --- --- # of CUDA cores: " << deviceProp.multiProcessorCount * _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor) << std::endl;
			std::cout << " --- clockRate: " << (double)deviceProp.clockRate / (1000 * 1000) << " GHz" << std::endl;
			std::cout << " --- Max # of threads per block: " << deviceProp.maxThreadsPerBlock << std::endl;
			std::cout << " --- Max # of threads per multi-processor: " << deviceProp.maxThreadsPerMultiProcessor << std::endl;
			std::cout << " --- Max dimension size of a thread block: (" << deviceProp.maxThreadsDim[0] << ", " << deviceProp.maxThreadsDim[1] << ", " << deviceProp.maxThreadsDim[2] << ")" << std::endl;
			std::cout << " --- Max dimension size of a grid size: (" << deviceProp.maxGridSize[0] << ", " << deviceProp.maxGridSize[1] << ", " << deviceProp.maxGridSize[2] << ")" << std::endl;
		}
		template <typename real> int getPeakFlops(const Prefix unit) const{
			return (double)(deviceProp.clockRate * 1000) * _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor) * deviceProp.multiProcessorCount / unit * 8.0 / sizeof(real);
		}
		std::size_t getPeakBandwidth(const Prefix unit) const{
			//Memory clock rate * bus width / 8 * 2 (dual?)
			return (deviceProp.memoryClockRate * 1e+3 * deviceProp.memoryBusWidth / 8 * 2) / unit;
		}
		template <typename real> flops StreamFlopsAdd(const std::size_t Nelem, const std::size_t Niter = 1) const{
			real *x_h, *y_h, *z_h, *x_d, *y_d, *z_d;
			std::cout << "Error code: " << cudaMallocHost((void**)&x_h, Nelem * sizeof(real)) << std::endl;
			std::cout << "Error code: " << cudaMallocHost((void**)&y_h, Nelem * sizeof(real)) << std::endl;
			std::cout << "Error code: " << cudaMallocHost((void**)&z_h, Nelem * sizeof(real)) << std::endl;
			std::cout << "Error code: " << cudaMalloc((void**)&x_d, Nelem * sizeof(real)) << std::endl;
			std::cout << "Error code: " << cudaMalloc((void**)&y_d, Nelem * sizeof(real)) << std::endl;
			std::cout << "Error code: " << cudaMalloc((void**)&z_d, Nelem * sizeof(real)) << std::endl;
			//set random num.
			double chksum_host = 0;
			srand(0);
			for(int i = 0 ; i < Nelem ; ++ i){
				x_h[i] = static_cast<real>(2.0 * (double)rand()/RAND_MAX - 1.0);
				y_h[i] = static_cast<real>(2.0 * (double)rand()/RAND_MAX - 1.0);
				z_h[i] = x_h[i];
				for(int it = 0 ; it < Niter ; ++ it){
					//z_h[i] += y_h[i];
				}
				chksum_host += (double)z_h[i];
			}
			//send data
			cudaMemcpy(x_d, x_h, Nelem * sizeof(real), cudaMemcpyHostToDevice);
			cudaMemcpy(y_d, y_h, Nelem * sizeof(real), cudaMemcpyHostToDevice);
			dim3 size_thread(deviceProp.maxThreadsDim[0], 1, 1);
			dim3 size_grid(Nelem / 1024, 1, 1);
			CudaTimer timer;
			timer.start();
			deviceStreamAdd<real> <<<size_grid, size_thread>>>(x_d, y_d, z_d, Nelem, Niter);
			timer.stop();
			std::cout << "Measured Performance = " << Nelem * Niter / timer.getWallclockTime() / 1.0e+9 << " GFLOPs, Wall clock time = " << timer.getWallclockTime() << "sec, Intensity = " << (double)Niter / (double)sizeof(real) << std::endl;
			cudaMemcpy(z_h, z_d, Nelem * sizeof(real), cudaMemcpyDeviceToHost);
			double chksum_dev = 0;
			for(int i = 0 ; i < Nelem ; ++ i){
				chksum_dev += (double)z_h[i];
			}
			std::cout << std::setprecision(16) << std::scientific << "chksum host / dev = " << chksum_host << " / " << chksum_dev << std::endl;
			cudaFreeHost(x_h);
			cudaFreeHost(y_h);
			cudaFreeHost(z_h);
			cudaFree(x_d);
			cudaFree(y_d);
			cudaFree(z_d);
		}
		template <typename real> bandwidth StreamCopy(const std::size_t Nelem) const{
			real *x_h, *y_h, *x_d, *y_d;
			std::cout << "Error code: " << cudaMallocHost((void**)&x_h, Nelem * sizeof(real)) << std::endl;
			std::cout << "Error code: " << cudaMallocHost((void**)&y_h, Nelem * sizeof(real)) << std::endl;
			std::cout << "Error code: " << cudaMalloc((void**)&x_d, Nelem * sizeof(real)) << std::endl;
			std::cout << "Error code: " << cudaMalloc((void**)&y_d, Nelem * sizeof(real)) << std::endl;
			//set random num.
			double chksum_x = 0;
			for(int i = 0 ; i < Nelem ; ++ i){
				x_h[i] = static_cast<real>(2.0 * (double)rand()/RAND_MAX - 1.0);
				chksum_x += (double)x_h[i];
			}
			//send data
			cudaMemcpy(x_d, x_h, Nelem * sizeof(real), cudaMemcpyHostToDevice);
			dim3 size_thread(deviceProp.maxThreadsDim[0], 1, 1);
			dim3 size_grid(Nelem / 1024, 1, 1);
			CudaTimer timer;
			timer.start();
			deviceStreamCopy<<<size_grid, size_thread>>>(x_d, y_d, Nelem);
			timer.stop();
			std::cout << "Measured Bandwidth = " << 2 * Nelem * sizeof(real) / timer.getWallclockTime() / 1.0e+9 << " GB/s, Wall clock time = " << timer.getWallclockTime() << std::endl;
			cudaMemcpy(y_h, y_d, Nelem * sizeof(real), cudaMemcpyDeviceToHost);
			double chksum_y = 0;
			for(int i = 0 ; i < Nelem ; ++ i){
				chksum_y += (double)y_h[i];
			}
			std::cout << "NOTE: " << chksum_x << " - " << chksum_y << " should be nearly machine epsilon." << std::endl;
			cudaFreeHost(x_h);
			cudaFreeHost(y_h);
			cudaFree(x_d);
			cudaFree(y_d);
			bandwidth result;
			result.wallclock_time = timer.getWallclockTime();
			result.byte_per_sec = 2 * Nelem * sizeof(real) / timer.getWallclockTime();
			return result;
		}
	};
	int numDevice;
	std::vector<GPU> gpu;
	public:
	GPUs(){
		cudaError_t hasDevice = cudaGetDeviceCount(&numDevice);
		#if 0
		if(hasDevice == cudaErrorNoDevice){
			std::cout << "NO GPU" << std::endl;
		}else{
			std::cout << cudaGetErrorString(hasDevice) << ", " << cudaErrorNoDevice << std::endl;
		}
		#endif
		std::cout << "# of devices: " << numDevice << std::endl;
		for(int i = 0 ; i < numDevice ; ++ i){
			cudaSetDevice(i);
			GPU device(i);
			gpu.push_back(device);
		}
		return ;
	}
	int getNumberOfDevice(){
		return numDevice;
	}
	GPUs getPlatform(const int i){
		return *this;
	}
	GPU getDevice(const int i){
		return gpu[i];
	}
};

int main(){
	GPUs system;
	system.getPlatform(0).getDevice(0).getQuery();
	const std::size_t Nelem = 1024 * 1024 * 16;
	const std::size_t Niter = 128*2;

	{
		std::cout << "=========================================" << std::endl;
		typedef half real;
		std::cout << "Peak Flops     : " << system.getPlatform(0).getDevice(0).getPeakFlops<real>(G) << std::endl;
		std::cout << "Peak Bandwidth : " << system.getPlatform(0).getDevice(0).getPeakBandwidth(G) << std::endl;
		system.getPlatform(0).getDevice(0).StreamFlopsAdd<real>(Nelem, Niter);
		system.getPlatform(0).getDevice(0).StreamCopy<real>(Nelem);
		std::cout << "=========================================" << std::endl;
	}
	/*
	{
		std::cout << "=========================================" << std::endl;
		typedef float real;
		std::cout << "Peak Flops     : " << system.getPlatform(0).getDevice(0).getPeakFlops<real>(G) << std::endl;
		std::cout << "Peak Bandwidth : " << system.getPlatform(0).getDevice(0).getPeakBandwidth(G) << std::endl;
		system.getPlatform(0).getDevice(0).StreamFlopsAdd<real>(Nelem, Niter);
		system.getPlatform(0).getDevice(0).StreamCopy<real>(Nelem);
		std::cout << "=========================================" << std::endl;
	}
	{
		std::cout << "=========================================" << std::endl;
		typedef double real;
		std::cout << "Peak Flops     : " << system.getPlatform(0).getDevice(0).getPeakFlops<real>(G) << std::endl;
		std::cout << "Peak Bandwidth : " << system.getPlatform(0).getDevice(0).getPeakBandwidth(G) << std::endl;
		system.getPlatform(0).getDevice(0).StreamFlopsAdd<real>(Nelem, Niter);
		system.getPlatform(0).getDevice(0).StreamCopy<real>(Nelem);
		std::cout << "=========================================" << std::endl;
	}
	*/
	return 0;
}

