#pragma once

template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	#if 0
	static const double visc = 1.0e-6;
	static const double size = 1.0e-3;
	static const double vel = 1.25e-5;
	static const double END_TIME = 800;
	#else
	static constexpr double visc = 1.0e-4;
	static constexpr double size = 0.2;
	static constexpr double vel = 1.25e-5 * 500;
	static constexpr double END_TIME = 20000;
	#endif
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		mpicin<int> N;
		const double dx = size / N.getValue();
		const double wall = dx * 3.0;
		const double length = size;
		const double comp = 0.005;//compression rate
		std::cout << sqrt(vel * vel / comp) << " " << sqrt(visc * vel / size / comp) << " " << 10 * vel << std::endl;
		const int Nwall = 4;
		std::size_t i = 0;

		static const Viscosity::Newtonian<PS::F64> Visc(visc);
		static const EoS::Murnaghan<PS::F64> EoS(7.0, 1000.0, vel * 10);

		for(int ix = 0 ; ix < N.getValue() ; ++ ix){
			for(int iy = - Nwall ; iy <= N.getValue() + Nwall ; ++ iy){
				const PS::F64 x = 0.0 + ix * dx;
				const PS::F64 y = 0.0 + iy * dx;
				++ i;
				Ptcl ith;
				if(iy <= 0){
					//set for boundary
					ith.type = FREEZE;
					ith.tag = 1;
				}else if(iy >= N.getValue()){
					//set for boundary
					ith.type = FREEZE;
					ith.tag = 2;
					ith.vel.x = vel;
				}else{
					ith.tag = 0;
				}
				ith.pos.x = x;
				ith.pos.y = y;
				ith.dens = EoS.ReferenceDensity();
				ith.mass = ith.dens * dx * dx;
				ith.id = i;
				ith.EoS = &EoS;
				ith.visc = &Visc;
				ptcl.push_back(ith);
			}
		}

		std::cout << "v_max: " << vel << std::endl;
		std::cout << "Re   : " << vel * size / Visc.KineticViscosity() << std::endl;
		std::cout << "Time : " << size / vel << std::endl;
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_X);
		dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0), PS::F64vec(size, size));
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
	}
	static void postTimestepProcess(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].type != FREEZE) continue;
			sph_system[i].pos += sph_system[i].vel * sysinfo.dt;
		}
	}
};

