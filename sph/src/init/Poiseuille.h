#pragma once

template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	#if 0
	static const double size = 1.0e-3;
	static const double force = 1.0e-4;
	static const double visc = 1.0e-6;
	static const double END_TIME = 1.0;
	#else
	static const double size = 0.2;
	static const double force = 1.2e-6 * 100;
	static const double visc = 1.0e-4;
	static const double END_TIME = 8000;
	#endif
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		mpicin<int> N;
		static const double v_max = force * size * size / (visc * 8.0);
		static const Viscosity::Newtonian<PS::F64> Visc(visc);
		static const EoS::Murnaghan<PS::F64> EoS(7.0, 1000.0, v_max * 10);
		const double comp = 0.005;//compression rate
		std::cout << sqrt(v_max * v_max / comp) << " " << sqrt(visc * v_max / size / comp) << " " << sqrt(force * size / comp) << " " << 10 * v_max << std::endl;
		const int Nwall = 3;
		const double dx = size / N.getValue();
		const double aspect_ratio = 8.0;// = x / y
		std::size_t i = 0;
		for(int ix = 0 ; ix < aspect_ratio * N.getValue() ; ++ ix){
			for(int iy = - Nwall ; iy <= N.getValue() + Nwall ; ++ iy){
				const PS::F64 x = 0.0 + ix * dx;
				const PS::F64 y = 0.0 + iy * dx;
				++ i;
				Ptcl ith;
				if(iy <= 0){
					//set for boundary
					ith.type = FREEZE;
					ith.tag = 1;
				}else if(iy >= N.getValue()){
					//set for boundary
					ith.type = FREEZE;
					ith.tag = 2;
				}else{
					ith.tag = 0;
				}
				ith.pos.x = x;
				ith.pos.y = y;
				ith.dens = EoS.ReferenceDensity();
				ith.mass = ith.dens * dx * dx;
				ith.id = i;
				ith.EoS = &EoS;
				ith.visc = &Visc;
				ptcl.push_back(ith);
			}
		}
		std::cout << "v_max: " << v_max << std::endl;
		std::cout << "Re   : " << v_max * size / Visc.KineticViscosity() << std::endl;
		std::cout << "Time : " << size / v_max << std::endl;
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_X);
		dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0), PS::F64vec(aspect_ratio * size, size));
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			if(sph_system[i].type == FREEZE) continue;
			sph_system[i].acc.x += force;
		}
	}
	static void postTimestepProcess(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo){
	}
};

